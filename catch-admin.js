jQuery(document).ready(function($){
    $('.catch_edit').on('click', function(e){
        e.preventDefault();
        var th = $(this).parent().parent(),
            i,
            data = [],
            total = th.children().length,
            myform = $('#catch_admin_form');


        th.css('background', '#a0d3e8');

        setTimeout( function(){
                th.css('background', '');
            }
            , 700 );

        for( i=0; i<( total - 1); i++ ){
            data[i] = th.find('th').eq(i).html();
        }

        data.join();

        myform.find('#catch_river').val(data[0]).attr('readonly', true);
        myform.find('#catch_code').val(data[1]).attr('readonly', true);
        myform.find('#low_flow').val(data[2]);
        myform.find('#optimal_flow').val(data[3]);
        myform.find('#high_flow').val(data[4]);
        myform.find('input[type="submit"]').val('Save');
        $('#catch-title').html('<button class="button button-primary" onClick="window.location.reload();">Add New</button><br/>');

        $("html, body").animate({ scrollTop: 0 }, "slow");

    });
});
