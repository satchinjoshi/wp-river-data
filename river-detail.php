<?php
/*
Plugin Name: River Detail
Version: 0.5
Author: Sachin Joshi
License: GPL2
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; /*Exit if accessed directly*/
}

define( 'CATCH_PLUGIN_URL' , plugin_dir_url( __FILE__ ));

if ( get_option( '_river_cache_data_catch' ) ) {
	$time = get_option( '_river_cache_data_catch' ) * 60 * 60;
} else {
	$time = 12 * 60 * 60;
}

/** Set the time for the cache, input the time in second */
define( 'CATCH_CACHE_THE_DATA_TIME' , $time );


$files = array(
	'class-shortcode',
	'class-admin',
);
foreach ( $files as $file ) {
	require_once 'include/' . $file . '.php';
}
