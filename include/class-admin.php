<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; /*Exit if accessed directly*/
}

class CatchAdminMainMenu
{

	public static function request( $data ) {
		if ( isset( $data ) && empty( $data ) ) {
			return false;
		}

		return esc_attr( trim( $data ) );
	}

	public static function admin_menu(){
		add_menu_page('River Detail', 'Rivers API', 'manage_options', 'catch-and-the-hatch-river', array('CatchAdminMainMenu', 'main_option_page'),  '', '99');
		add_submenu_page( 'catch-and-the-hatch-river', 'Setting', 'Setting', 'manage_options', 'catch-and-the-hatch-river-setting',  array('CatchAdminMainMenu', 'sub_option_page') );
		add_submenu_page( 'catch-and-the-hatch-river', 'Docs', 'Docs', 'manage_options', 'catch-and-the-hatch-river-docs',  array('CatchAdminMainMenu', 'documentation') );
	}


	public static function sub_option_page(){

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		if ( $_POST ) {
			$time = self::request( $_POST['cache_time'] );
			if( $time != false && absint($time)){
				update_option('_river_cache_data_catch', absint($time) );
			}
		}

		$time = get_option('_river_cache_data_catch');

		?>
		<h3>Refresh Time for Data</h3>
		<i>Enter the time in hour</i>
		<form action="admin.php?page=catch-and-the-hatch-river-setting" method="POST">
			<table>
				<tr>
					<th><label for="cache_time">Cache Time:</label></th>
					<td><input type="text" name="cache_time" id="cache_time" value="<?php echo $time; ?>"/></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" class="button button-primary" value="Save"/></td>
				</tr>
			</table>
		</form>
		<?php
	}

	/**
	 *  Main Admin Page
	 * 	Its also handles data
	 */
	public static function main_option_page(){
		global $catch_saved;

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		if($_POST) self::save_river_code_data();

		if( self::request( $_GET['act'] ) == 'del' && self::request( $_GET['key'] ) && self::request( $_GET['val'] ) ){
			self::del_data( self::request( $_GET['key'] ) );
		}

	?>
		<h3>Input Field</h3>
		<div id="catch-title"></div>
		<i>Note:- Enter the river name for shortcode and enter the code that is going to used.</i><br/>
		<i>*:- These field cannot be edited later. You need to delete it for change.</i>
		<form action="admin.php?page=catch-and-the-hatch-river" id="catch_admin_form" method="POST">
			<table>
				<tr>
					<th><label for="catch_river">River Name*:</label></th>
					<td><input type="text" name="catch_river" id="catch_river" value="<?php echo ($catch_saved === false)?self::request($_POST['catch_river']):''; ?>"/></td>
					<td><i>Your river name choice for shortcode</i></td>
				</tr>
				<tr>
					<th><label for="catch_code">Code Name*:</label></th>
					<td><input type="text" name="catch_code" id="catch_code" value="<?php echo ($catch_saved === false)?self::request($_POST['catch_code']):''; ?>" /></td>
					<td><i>Actual abbr or code or site code</i></td>
				</tr>
				<tr>
					<th><label for="low_flow">Low Flow:</label></th>
					<td><input type="text" id="low_flow" name="low_flow" value="<?php echo ($catch_saved === false)?self::request($_POST['low_flow']):''; ?>"/></td>
				</tr>
				<tr>
					<th><label for="optimal_flow">Optimal Flow:</label></th>
					<td><input type="text" id="optimal_flow" name="optimal_flow" value="<?php echo ($catch_saved === false)?self::request($_POST['optimal_flow']):''; ?>"/></td>
				</tr>
				<tr>
					<th><label for="high_flow">High Flow:</label></th>
					<td><input type="text" id="high_flow" name="high_flow" value="<?php echo ($catch_saved === false)?self::request($_POST['high_flow']):''; ?>"/></td>
				</tr>
				<tr>
					<td></td><td><input type="submit" name="catch_submit" class="button button-primary" value="Add"/></td>
				</tr>
			</table>
		</form>

		<?php $data = get_option( 'catch_river_code' ); ?>
		<?php if( $data == true ): ?><!--if option exists-->
		<!-- this is the field for table list-->
		<h3>Data list</h3>
			<table class="wp-list-table widefat fixed pages">
					<thead>
						<tr>
							<th><strong>Name For Short Code<strong></th>
							<th><strong>River ID or Code<strong></th>
							<th><strong>Low Flow<strong></th>
							<th><strong>Optimal Flow<strong></th>
							<th><strong>High Flow<strong></th>
							<th><strong>Action<strong></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><strong>Name For Short Code<strong></th>
							<th><strong>River ID or Code<strong></th>
							<th><strong>Low Flow<strong></th>
							<th><strong>Optimal Flow<strong></th>
							<th><strong>High Flow<strong></th>
							<th><strong>Action<strong></th>
						</tr>
					</tfoot>
				<?php $i = 0; ksort($data); ?>
				<?php foreach ( $data as $key => $value ): ?>
					<?php if($key == '') continue; ?>
					<tr <?php echo ( $i % 2 ) ? 'class="alternate "' : ''; ?>>
						<th><?php $metas = ''; echo $key; $metas[] = $key; ?></th>
							<?php foreach ( $value as $val ): ?>
								<th><?php echo $val; $metas[] = $val ?></th>
							<?php endforeach; ?>
						<th>
							<a href="admin.php?page=catch-and-the-hatch-river&act=del&key=<?php echo $key.'&val='.$value; ?>">Delete</a>
							| <a href="" class="catch_edit">Edit</a>
						</th>
					</tr>
				<?php $i++; endforeach; ?>
			</table>
		<?php endif; ?>
	<?php
	}

	public static function save_river_code_data() {
		global $catch_saved;
		if ( self::request( $_POST['catch_code'] ) === false || self::request( $_POST['catch_river'] ) === false ) {
			echo '<div class="error">Please fill all the input.</div>';
			$catch_saved = false;
			return;
		}

		$data = get_option( 'catch_river_code' );

		$key     = self::request( $_POST['catch_river'] );
		$value   = self::request( $_POST['catch_code'] );
		$low     = self::request( $_POST['low_flow'] );
		$optimal = self::request( $_POST['optimal_flow'] );
		$high    = self::request( $_POST['high_flow'] );


		if (  is_array( $data )  ) {

			/*check for duplicate key (river)*/
			if ( array_key_exists( $key, $data ) ) {

				/* check if it is edited */
				if ( self::request( $_POST['catch_submit'] ) == 'Save' ) {
					unset( $data[ $key ] );
					if ( update_option( 'catch_river_code', $data ) === true ) {
						$catch_saved = true;
					}
				} else {
					echo "<div class='error'>River Name Already Exists</div>";
					$catch_saved = false;

					return;
				}

			}

			$data[ $key ] = array(
				'river_code' => $value,
				'low' => $low,
				'optimal' => $optimal,
				'high' => $high,
			);


			/*save the data*/
			if ( update_option( 'catch_river_code', $data ) === true ) {
				$catch_saved = true;
				echo "<div id='message' class='updated'>Saved</div>";
			} else {
				$catch_saved = false;
				echo "<div class='error'>Not saved something went wrong</div>";
			}

		} elseif ( $data === false ){/*for first data */

			$data[ $key ] = array(
				'river_code' => $value,
				'low' => $low,
				'optimal' => $optimal,
				'high' => $high,
			);

			if ( update_option( 'catch_river_code',  $data ) === true ) {
				$catch_saved = true;
				echo "<div id='message' class='updated'>Saved</div>";
			} else {
				$catch_saved = false;
				echo "<div class='error'>Not saved something went wrong</div>";
			}
		}
	}

	public static function del_data( $key ) {
		$data = get_option( 'catch_river_code' );
		if ( array_key_exists( $key, $data ) ) {
			unset( $data[ $key ] );

			if ( update_option( 'catch_river_code', $data ) === true ) {
				echo "<div id='message' class='updated'>Deleted</div>";
			} else {
				echo "<div class='error'>Something went wrong</div>";
			}

		}
	}


	public static function catch_load_custom_script(){
		global $pagenow;
		if( $pagenow != 'admin.php' )
			return;
		wp_enqueue_script( 'catch-cutom-script', CATCH_PLUGIN_URL . 'catch-admin.js', array( 'jquery' ) );
	}

	public static function documentation(){
		?>
		<h3>Documentation:</h3>
		<ui>
			<li>General:</li>
			<strong>
				 <p><a href="admin.php?page=catch-and-the-hatch-river">Go to this page </a> and set the river name to be used for the shortcode.</p>
				<p>But the river id must be exact. And set the river flow. </p>
				<p>You can delete the river or edit them but cannot change the river name and code.</p>
				<p>If you want to change it delete it and add new.</p>
			</strong>
			<li>ShortCode:</li>
			<p><strong>Use shortcode [catchriver display='text' river='Your choice of river name'] or [catchriver
					display='graph' river='Your choice of river name']</strong></p>
			<li>Data Cache time:</li>
			<p><strong>
					You can set data refresh time yourself in hour.
					<a href="admin.php?page=catch-and-the-hatch-river-setting">Click here to set cache time for data</a>
				</strong>
			</p>
		</ui>
		<?php
	}
}

add_action('admin_menu', array('CatchAdminMainMenu', 'admin_menu'));
add_action( 'admin_enqueue_scripts',  array('CatchAdminMainMenu', 'catch_load_custom_script') );
