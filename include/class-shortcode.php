<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; /*Exit if accessed directly*/
}

class CatchShortCode {

	public static function catch_shortcode( $atts ) {
		global $global_current_river;
		$html = '';

		$river_id = self::river_id( $atts['river'] );

		if ( $river_id === false ) {
			return "<strong>Error:</strong> No Such River";
		}

		$river_data = self::riv_meta_gather( $river_id );

		/*If the id does not pass the 8 digit validation return error*/
		if ( $river_data == false ) {

			return "<strong>Error:</strong> Something went wrong! Please refresh the page or come back later";

		} elseif ( isset( $atts['river'] ) && !empty( $atts['river'] ) ) {

			if ( $atts['display'] == 'graph' ) {

				$html .= '<div class="river-graph" >';
				$html .= '<img src="' . $river_data['graph_url'] . '" width="576" height="400" alt="USGS Water-data graph for site '.$river_id.'" />';

			} elseif ( $atts['display'] == 'text' ) {

				$html .= '<div class="river-text" >';

				$html .= '<strong>Current Water Level: ' . $river_data['current_water_level'] .' cfs</strong>';

			}


			if ( is_array( $global_current_river ) ) {

				if ( isset( $global_current_river['low'] ) ) {
					$html .= '<div class="low-flow">
										<p>Low Flows: ' . $global_current_river['low'] . ' cfs</p>
							  </div>';
				}
				if ( isset( $global_current_river['optimal'] ) ) {
					$html .= '<div class="optimal-flow">
										<p>Optimal Flows: ' . $global_current_river['optimal'] . ' cfs</p>
							</div>';
				}
				if ( isset( $global_current_river['high'] ) ) {
					$html .= '<div class="high-flow">
										<p>High Flows: ' . $global_current_river['high'] . ' cfs</p>
							</div>';
				}

			}

			$html .= '</div>';

			return $html;
		}
	}


	public static function river_id( $river_name ) {

		$data       = get_option( 'catch_river_code' );
		$river_code = null;
		global $global_current_river;
		foreach ( $data as $key => $value ) {
			if ( $key == $river_name ) {
				$global_current_river = $value;
				$river_code = $value['river_code'];
				continue;
			}
		}

		return ( $river_code === null ) ? false : $river_code;
	}

	/*Validate the USGS number to ensure it a string of 8 numbers*/
	public static function river_id_validate( $river_usgs_id ) {

		if ( preg_match( '/^\d{8}$/', $river_usgs_id ) ) {
			return true;
		} else {
			return false;
		}
	}

	public static function riv_meta_gather( $river_usgs_id ) {

		/* Validate the River USGS ID Provided by User*/
		if ( ! self::river_id_validate( $river_usgs_id ) ) {
			return false;
		}

		/* get the cache if saved */
		$riverCache = self::get_river_cache( $river_usgs_id );
		if ( $riverCache ) {
			return $riverCache;
		}

		/* Construct the Station URL and Graph URL */
		$station_url = "http://waterdata.usgs.gov/nwis/uv?" . $river_usgs_id;
		$graph_url   = "http://waterdata.usgs.gov/nwisweb/graph?site_no=" . $river_usgs_id . "&parm_cd=00060";
		$gage_url    = "http://waterdata.usgs.gov/nwisweb/graph?site_no=" . $river_usgs_id . "&parm_cd=00065";

		/* Define USGS Constants and send Get Request*/
		$api_url        = "http://waterservices.usgs.gov/nwis/iv?sites=";
		$api_url_ending = "&parameterCd=00060,00065&format=json";
		$api_response   = wp_remote_get( $api_url . $river_usgs_id . $api_url_ending );

		/* Get JSON object*/
		$json = wp_remote_retrieve_body( $api_response );

		/* Make sure the request was successful or return false*/
		if ( empty( $json ) ) {
			return false;
		}

		/*Decode JSON
		Return an array with the river name and current water level*/
		$json = json_decode( $json );

		if ( !isset( $json->value->timeSeries[0]->sourceInfo->siteName ) ) {
			return false;
		}

		$usgsData = array(
			'river_name'          => $json->value->timeSeries[0]->sourceInfo->siteName,
			'river_id'            => $river_usgs_id,
			'current_water_level' => $json->value->timeSeries[0]->values[0]->value[0]->value,
			'current_gage_height' => $json->value->timeSeries[1]->values[0]->value[0]->value,
			'station_url'         => $station_url,
			'graph_url'           => $graph_url,
			'gage_url'            => $gage_url,
			'time'                => time(),
		);

		/* save the data to cache for re-use*/
		self::save_river_cache( $usgsData );

		return $usgsData;
	}

	public static function save_river_cache( $data ) {
		$transient = '_river_' . $data['river_id'];
		set_transient( $transient, $data, CATCH_CACHE_THE_DATA_TIME );
	}

	/**
	 *  this methods check the date and data
	 *  if exist or is of around certain time
	 *  @param $id
	 *  @return array | bool
	 */
	public static function get_river_cache($id){

		if( ( $data = get_transient( '_river_'.$id ) ) == false ) return false;

		return $data;
	}
}

add_shortcode( 'catchriver', array( 'CatchShortCode', 'catch_shortcode' ) );
